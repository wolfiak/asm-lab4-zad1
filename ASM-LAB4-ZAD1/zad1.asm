.386
.MODEL flat,STDCALL

STD_INPUT_HANDLE equ -10
STD_OUTPUT_HANDLE equ -11

GetStdHandle PROTO :DWORD
ExitProcess PROTO : DWORD
wsprintfA PROTO C :VARARG
WriteConsoleA PROTO : DWORD, :DWORD, :DWORD, :DWORD, :DWORD
ReadConsoleA  PROTO :DWORD, :DWORD, :DWORD, :DWORD, :DWORD
atoi  PROTO :DWORD
.DATA
		cout		   dd ?
		cin			   dd ?
		tekst          db "Wprowadz pierwsza liczbe: ",0
		rozmiart       dd $ - tekst
		tekst2         db "Wprowadz druga liczbe: ",0
		rozmiart2      dd $ - tekst2
		tekst3         db "Wprowadz trzecia liczbe: ",0
		rozmiart3      dd $ - tekst3
		tekst4         db "Wprowadz czwarta liczbe: ",0
		rozmiart4      dd $ - tekst4
		liczba         dd 0
		bufor          db 128 DUP(0)
		bufor2         db 128 DUP(0)
		bufor3         db 2 DUP(0)
		bufor4         db 2 DUP(0)
		zm             db ?
		zm2            db ?
		zm3            db ?
		zm4            db ?
		liczbaZ        dd 0
		rozmiar		   dd ?
		teksto         db 128 DUP(0)
.CODE
main proc
	invoke GetStdHandle, STD_OUTPUT_HANDLE
	mov cout, EAX
	
	invoke GetStdHandle, STD_INPUT_HANDLE
	mov cin, EAX

	invoke WriteConsoleA, cout, OFFSET tekst, rozmiart, OFFSET liczba, 0
	invoke ReadConsoleA, cin, OFFSET bufor, 4, OFFSET liczba, 0
	LEA EBX, bufor
	mov EDI,liczba
	mov BYTE PTR [EBX+EDI-2],0
		invoke atoi, OFFSET bufor
;	cld
;	lea EDI, bufor
;	mov ECX, (SIZEOF bufor)
;	mov al,0
;	rep stosb
;	invoke atoi, OFFSET bufor
;	mov liczbaZ,0
;	mov liczba, 0 
;	mov bufor, 0'
	mov zm, AL
	invoke WriteConsoleA, cout, OFFSET tekst2, rozmiart2, OFFSET liczba, 0

	invoke ReadConsoleA, cin, OFFSET bufor, 4, OFFSET liczbaZ, 0
	LEA EBX, bufor
	mov EDI,liczbaZ
	mov BYTE PTR [EBX+EDI-2],0
	invoke atoi, OFFSET bufor
	mov zm2, AL

	invoke WriteConsoleA, cout, OFFSET tekst3, rozmiart3, OFFSET liczba, 0
	invoke ReadConsoleA, cin, OFFSET bufor, 4, OFFSET liczbaZ, 0
	LEA EBX, bufor
	mov EDI,liczbaZ
	mov BYTE PTR [EBX+EDI-2],0
	invoke atoi, OFFSET bufor
	mov zm3, AL
	
	invoke WriteConsoleA, cout, OFFSET tekst4, rozmiart4, OFFSET liczba, 0
	invoke ReadConsoleA, cin, OFFSET bufor2, 4, OFFSET liczbaZ, 0
	LEA EBX, bufor2
	mov EDI,liczbaZ
	mov BYTE PTR [EBX+EDI-2],0
	invoke atoi, OFFSET bufor2
	mov zm4, AL


	invoke wsprintfA, OFFSET teksto, OFFSET bufor
	mov rozmiar,EAX
	invoke WriteConsoleA, cout,OFFSET teksto, rozmiar, OFFSET liczba, 0

	invoke ExitProcess, 0



main endp
atoi proc uses esi edx inputBuffAddr:DWORD
	mov esi, inputBuffAddr
	xor edx, edx
	xor EAX, EAX
	mov AL, BYTE PTR [esi]
	cmp eax, 2dh
	je parseNegative

	.Repeat
		
		lodsb
		.Break .if !eax
		imul edx, edx, 10
		sub eax, "0"
		add edx, eax
	.Until 0
	mov EAX, EDX
	jmp endatoi

	parseNegative:
	inc esi
	.Repeat
		lodsb
		.Break .if !eax
		imul edx, edx, 10
		sub eax, "0"
		add edx, eax
	.Until 0

	xor EAX,EAX
	sub EAX, EDX
	jmp endatoi

	endatoi:
	ret
atoi endp
END